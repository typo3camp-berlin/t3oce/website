'use strict';

// Require the path module
const path = require('path');

// Create a new Fractal instance and export it for use elsewhere if required
const fractal = (module.exports = require('@frctl/fractal').create());

// Set the title of the project
fractal.set('project.title', 't3oce');

// Tell Fractal where the components are
fractal.components.set('path', 'src/components');

// Tell Fractal where the documentation pages are
fractal.docs.set('path', 'src/docs');

// Specify a directory of static assets
fractal.web.set('static.path', 'src/public');

// Set the static HTML build destination
fractal.web.set('builder.dest', 'dist');
