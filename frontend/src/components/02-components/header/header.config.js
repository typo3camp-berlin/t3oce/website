module.exports = {
  title: 'Page Header',
  status: 'wip',
  preview: '@styleguide',
  context: {
    title: 'Unterseiten Header',
    text: 'In diesem Jahr hat die besondere Situation leider dazu geführt, dass wir uns alle voraussichtlich nur aus der Ferne sehen und treffen können. Wir wollen euch deshalb eine Plattform bieten, auf der ihr euch austauschen, entwickeln und einfach Spaß zusammen haben könnt. Deshalb findet vom findet vom 01. bis 02. August das erste gemeinsame “TYPO3 Online Community Event” (#t3oce) statt.',
  },
};
