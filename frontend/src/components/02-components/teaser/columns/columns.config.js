module.exports = {
  title: 'Teaser columns',
  status: 'ready',
  preview: '@styleguide',
  context: {
    hasHeadlineInside: true,
  },
};
