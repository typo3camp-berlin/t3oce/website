module.exports = {
  title: 'Banner',
  status: 'ready',
  preview: '@styleguide',
  context: {
    headline: 'Spenden-Projekt',
    showGrid: false,
    showLogos: false,
    showBackgroundImage: true,
    hasNoBackground: false,
    fontColorWhite: true,
  },
  variants: [
    {
      name: 'Banner with logos',
      context: {
        headline: 'Wer sind wir?',
        showLogos: true,
      },
    },
    {
      name: 'Banner with grid',
      context: {
        headline: 'Spenden-Projekte',
        showGrid: true,
      },
    },
    {
      name: 'Banner without background',
      context: {
        headline: 'Banner',
        showBackgroundImage: false,
        hasNoBackground: true,
        fontColorWhite: false,
      },
    },
  ],
};
