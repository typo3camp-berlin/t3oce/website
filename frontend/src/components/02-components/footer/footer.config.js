module.exports = {
  title: 'Footer',
  status: 'ready',
  preview: '@styleguide',
  context: {
    items: [
      {
        label: 'Kontakt',
      },
      {
        label: 'Impressum',
      },
      {
        label: 'Datenschutz',
      },
    ],
  },
};
