# Frontend for T3OCE

You need npm/node and yarn to be installed on your local machine.

First of all run `npm install` to install all necessary files needed for the frontend setup.

## Starting the fractal locally

To start the fractal run `yarn start`.
To see the changes made on the handlebar files immediately, run `fractal start --sync` instead of `yarn start`.

If you are working on the scss files and wish to see the changes immediately, run `yarn scss` in a second terminal.

## Copy the build files to EXT:t3oce_sitepackage

After finishing the work on the frontend files you can run `yarn build` to get the files copied to the
sitepackage extension.

With this command all files inside the folder `frontend/src/public` will get copied to
`web/typo3conf/ext/t3oce_sitepackage/Resources/Public/Build`.