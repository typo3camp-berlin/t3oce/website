<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

// Add static files
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    't3oce_sitepackage',
    'Configuration/TypoScript',
    't3oce: Sitepackage'
);

// Allow new record type
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages(
    'tx_t3ocesitepackage_domain_model_content'
);
