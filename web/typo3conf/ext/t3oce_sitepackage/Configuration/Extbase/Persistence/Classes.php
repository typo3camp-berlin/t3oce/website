<?php

return [
    \T3oce\T3oceSitepackage\Domain\Model\Room::class => [
        'tableName' => 'tx_sessionplaner_domain_model_room',
        'properties' => [
            'streamLink' => [
                'fieldName' => 'tx_t3ocesitepackage_stream_link'
            ],
        ]
    ],
];