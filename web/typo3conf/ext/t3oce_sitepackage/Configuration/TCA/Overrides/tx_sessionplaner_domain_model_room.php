<?php
defined('TYPO3_MODE') or die();

call_user_func(function () {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_sessionplaner_domain_model_room', [
        'tx_t3ocesitepackage_stream_link' => [
            'exclude' => true,
            'label' => 'LLL:EXT:t3oce_sitepackage/Resources/Private/Language/locallang_db.xlf:tx_sessionplaner_domain_model_room.tx_t3ocesitepackage_stream_link',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputLink',
            ],
        ],
    ]);

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tx_sessionplaner_domain_model_room',
        'tx_t3ocesitepackage_stream_link'
    );
});