<?php
defined('TYPO3_MODE') or die();

call_user_func(function () {
    // Define project variables
    $projectExtensionkeyPrefix = 'EXT:t3oce_sitepackage/';
    $projectLanguageFilePrefix = 'LLL:' . $projectExtensionkeyPrefix . 'Resources/Private/Language/locallang_db.xlf:';

    // TYPO3 core language files
    $cmsLanguageFilePrefix = 'LLL:EXT:cms/locallang_ttc.xlf:';
    $coreLanguageFilePrefix = 'LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:';
    $databaseLanguageFilePrefix = 'LLL:EXT:frontend/Resources/Private/Language/Database.xlf:';
    $frontendLanguageFilePrefix = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:';
    $languageLanguageFilePrefix = 'LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:';

    // TYPO3 core tabs
    $coreTabAccess = '--div--;' . $coreLanguageFilePrefix . 'access,';
    $coreTabAccessibility = '--div--;' . $frontendLanguageFilePrefix . 'tabs.accessibility,';
    $coreTabAppearance = '--div--;' . $frontendLanguageFilePrefix . 'tabs.appearance,';
    $coreTabCategories = '--div--;' . $coreLanguageFilePrefix . 'categories,';
    $coreTabExtended = '--div--;' . $coreLanguageFilePrefix . 'extended,';
    $coreTabGeneral = '--div--;' . $coreLanguageFilePrefix . 'general,';
    $coreTabImages = '--div--;' . $frontendLanguageFilePrefix . 'tabs.images,';
    $coreTabLanguage = '--div--;' . $coreLanguageFilePrefix . 'language,';
    $coreTabMedia = '--div--;' . $frontendLanguageFilePrefix . 'tabs.media,';
    $coreTabNotes = '--div--;' . $coreLanguageFilePrefix . 'notes,';

    // TYPO3 core palettes
    $corePaletteAccess = '--palette--;' . $frontendLanguageFilePrefix . 'palette.access;access,';
    $corePaletteAppearanceLinks = '--palette--;' . $frontendLanguageFilePrefix . 'palette.appearanceLinks;appearanceLinks,';
    $corePaletteFrames = '--palette--;' . $frontendLanguageFilePrefix . 'palette.frames;frames,';
    $corePaletteGallerySettings = '--palette--;' . $databaseLanguageFilePrefix . 'tt_content.palette.gallerySettings;gallerySettings,';
    $corePaletteGeneral = '--palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,';
    $corePaletteHeader = '--palette--;' . $frontendLanguageFilePrefix . 'palette.header;header,';
    $corePaletteHeaders = '--palette--;' . $frontendLanguageFilePrefix . 'palette.headers;headers,';
    $corePaletteHidden = '--palette--;;hidden,';
    $corePaletteImagelinks = '--palette--;' . $frontendLanguageFilePrefix . 'palette.imagelinks;imagelinks,';
    $corePaletteLanguage = '--palette--;;language,';
    $corePaletteMediaAdjustments = '--palette--;' . $databaseLanguageFilePrefix . 'tt_content.palette.mediaAdjustments;mediaAdjustments,';
    $corePaletteMenuAccessibility = '--palette--;' . $frontendLanguageFilePrefix . 'palette.menu_accessibility;menu_accessibility,';

    /*
     * Add new fields and palette for section link
     */
    $tcaSectionLink = [
        'columns' => [
            'tx_t3ocesitepackage_bodytext2' => [
                'exclude' => true,
                'l10n_mode' => 'prefixLangTitle',
                'label' => $ll . 'tt_content.tx_t3ocesitepackage_bodytext2',
                'config' => [
                    'type' => 'text',
                    'cols' => '80',
                    'rows' => '15',
                    'softref' => 'typolink_tag,images,email[subst],url',
                    'enableRichtext' => true,
                    'richtextConfiguration' => 'default',
                ],
            ],
            'tx_t3ocesitepackage_bodytext3' => [
                'exclude' => true,
                'l10n_mode' => 'prefixLangTitle',
                'label' => $ll . 'tt_content.tx_t3ocesitepackage_bodytext3',
                'config' => [
                    'type' => 'text',
                    'cols' => '80',
                    'rows' => '15',
                    'softref' => 'typolink_tag,images,email[subst],url',
                    'enableRichtext' => true,
                    'richtextConfiguration' => 'default',
                ],
            ],
            'tx_t3ocesitepackage_content_elements' => [
                'label' => $projectLanguageFilePrefix . 'tt_content.tx_t3ocesitepackage_content_elements',
                'exclude' => '1',
                'config' => [
                    'type' => 'inline',
                    'foreign_table' => 'tx_t3ocesitepackage_domain_model_content',
                    'foreign_field' => 'parentid',
                    'foreign_table_field' => 'parenttable',
                    'foreign_sortby' => 'sorting',
                    'appearance' => [
                        'collapseAll' => true,
                        'expandSingle' => '1',
                        'newRecordLinkTitle' => 'New Tab Content',
                        'levelLinksPosition' => 'top',
                        'showPossibleLocalizationRecords' => true,
                        'showRemovedLocalizationRecords' => true,
                        'showAllLocalizationLink' => true,
                        'showSynchronizationLink' => true,
                        'enabledControls' => [
                            'info' => true,
                            'new' => true,
                            'dragdrop' => true,
                            'sort' => false,
                            'hide' => true,
                            'delete' => true,
                            'localize' => true,
                        ],
                        'useSortable' => '1',
                    ],
                    'minitems' => '0',
                    'maxitems' => '99',
                    'behaviour' => [
                        'localizeChildrenAtParentLocalization' => true,
                    ],
                ],
            ],
            'tx_t3ocesitepackage_sectionlink_title' => [
                'exclude' => true,
                'l10n_mode' => 'prefixLangTitle',
                'label' => $projectLanguageFilePrefix . 'tt_content.tx_t3ocesitepackage_sectionlink_title',
                'config' => [
                    'type' => 'input',
                    'size' => 50,
                    'max' => 255,
                ],
            ],
            'tx_t3ocesitepackage_sectionlink_url' => [
                'exclude' => true,
                'label' => $projectLanguageFilePrefix . 'tt_content.tx_t3ocesitepackage_sectionlink_url',
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputLink',
                    'size' => 50,
                    'max' => 1024,
                    'eval' => 'trim',
                    'fieldControl' => [
                        'linkPopup' => [
                            'options' => [
                                'title' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_link_formlabel',
                            ],
                        ],
                    ],
                    'softref' => 'typolink',
                ],
            ],
        ],
        'palettes' => [
            'sectionlink' => [
                'label' => $projectLanguageFilePrefix . 'tt_content.palettes.sectionlink',
                'showitem' => '
                    tx_t3ocesitepackage_sectionlink_title,
                    --linebreak--, tx_t3ocesitepackage_sectionlink_url
                ',
            ],
        ],
    ];
    $GLOBALS['TCA']['tt_content'] = array_replace_recursive(
        $GLOBALS['TCA']['tt_content'],
        $tcaSectionLink
    );

    /*
     * CType 'ce_disturber'
     */
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
        $projectLanguageFilePrefix . 'tt_content.CType.ce_disturber.title',
        'ce_disturber',
        '',
    ];
    $GLOBALS['TCA']['tt_content']['types']['ce_disturber'] = [
        'showitem' => '
            ' . $coreTabGeneral
                . $corePaletteGeneral
                . $corePaletteHeader
                . 'tx_t3ocesitepackage_content_elements,'
            . $coreTabAppearance
                . $corePaletteFrames
                . $corePaletteAppearanceLinks
            . $coreTabLanguage
                . $corePaletteLanguage
            . $coreTabAccess
                . $corePaletteHidden
                . $corePaletteAccess
            . $coreTabCategories
                . 'categories,'
            . $coreTabNotes
                . 'rowDescription,'
            . $coreTabExtended
            .'
        ',
        'columnsOverrides' => [
        ],
    ];

    /*
     * CType 'ce_header'
     */
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
        $projectLanguageFilePrefix . 'tt_content.CType.ce_header.title',
        'ce_header',
        '',
    ];
    $GLOBALS['TCA']['tt_content']['types']['ce_header'] = [
        'showitem' => '
            ' . $coreTabGeneral
                . $corePaletteGeneral
                . $corePaletteHeader
                . 'bodytext,'
            . $coreTabImages
                . 'image,'
            . $coreTabAppearance
                . $corePaletteFrames
                . $corePaletteAppearanceLinks
            . $coreTabLanguage
                . $corePaletteLanguage
            . $coreTabAccess
                . $corePaletteHidden
                . $corePaletteAccess
            . $coreTabCategories
                . 'categories,'
            . $coreTabNotes
                . 'rowDescription,'
            . $coreTabExtended
            .'
        ',
        'columnsOverrides' => [
            'bodytext' => [
                'config' => [
                    'enableRichtext' => true,
                ]
            ],
            'header' => [
                'config' => [
                    'required' => true,
                ],
            ],
            'image' => [
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
                    'maxitems' => 1,
                ], $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
            ],
        ],
    ];

    /*
     * CType 'ce_hero'
     */
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
        $projectLanguageFilePrefix . 'tt_content.CType.ce_hero.title',
        'ce_hero',
        '',
    ];
    $GLOBALS['TCA']['tt_content']['types']['ce_hero'] = [
        'showitem' => '
            ' . $coreTabGeneral
                . $corePaletteGeneral
                . $corePaletteHeaders
            . $coreTabImages
                . 'image,'
            . $coreTabAppearance
                . $corePaletteFrames
                . $corePaletteAppearanceLinks
            . $coreTabLanguage
                . $corePaletteLanguage
            . $coreTabAccess
                . $corePaletteHidden
                . $corePaletteAccess
            . $coreTabCategories
                . 'categories,'
            . $coreTabNotes
                . 'rowDescription,'
            . $coreTabExtended
            .'
        ',
        'columnsOverrides' => [
            'header' => [
                'config' => [
                    'required' => true,
                ],
            ],
            'image' => [
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
                    'minitems' => 1,
                    'maxitems' => 1,
                ], $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
            ],
        ],
    ];

    /*
     * CType 'ce_logobar'
     */
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
        $projectLanguageFilePrefix . 'tt_content.CType.ce_logobar.title',
        'ce_logobar',
        '',
    ];
    $GLOBALS['TCA']['tt_content']['types']['ce_logobar'] = [
        'showitem' => '
            ' . $coreTabGeneral
                . $corePaletteGeneral
                . $corePaletteHeader
                . 'bodytext,'
            . $coreTabImages
                . 'image,'
            . $coreTabAppearance
                . $corePaletteFrames
                . $corePaletteAppearanceLinks
            . $coreTabLanguage
                . $corePaletteLanguage
            . $coreTabAccess
                . $corePaletteHidden
                . $corePaletteAccess
            . $coreTabCategories
                . 'categories,'
            . $coreTabNotes
                . 'rowDescription,'
            . $coreTabExtended
            .'
        ',
        'columnsOverrides' => [
            'bodytext' => [
                'config' => [
                    'enableRichtext' => true,
                ]
            ],
        ],
    ];

    /*
     * CType 'ce_teaser_columns'
     */
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
        $projectLanguageFilePrefix . 'tt_content.CType.ce_teaser_columns.title',
        'ce_teaser_columns',
        '',
    ];
    $GLOBALS['TCA']['tt_content']['types']['ce_teaser_columns'] = [
        'showitem' => '
            ' . $coreTabGeneral
                . $corePaletteGeneral
                . $corePaletteHeader
                . 'bodytext;' . $projectLanguageFilePrefix . 'tt_content.CType.ce_teaser_columns.bodytext_formlabel,'
                . 'tx_t3ocesitepackage_bodytext2;' . $projectLanguageFilePrefix . 'tt_content.CType.ce_teaser_columns.tx_t3ocesitepackage_bodytext2_formlabel,'
                . 'tx_t3ocesitepackage_bodytext3;' . $projectLanguageFilePrefix . 'tt_content.CType.ce_teaser_columns.tx_t3ocesitepackage_bodytext3_formlabel,'
                . '--palette--;;sectionlink,'
            . $coreTabAppearance
                . $corePaletteFrames
                . $corePaletteAppearanceLinks
            . $coreTabLanguage
                . $corePaletteLanguage
            . $coreTabAccess
                . $corePaletteHidden
                . $corePaletteAccess
            . $coreTabCategories
                . 'categories,'
            . $coreTabNotes
                . 'rowDescription,'
            . $coreTabExtended
            .'
        ',
        'columnsOverrides' => [
            'bodytext' => [
                'config' => [
                    'enableRichtext' => true,
                ],
            ],
        ],
    ];

    /*
     * CType 'ce_teaser_element'
     */
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
        $projectLanguageFilePrefix . 'tt_content.CType.ce_teaser_element.title',
        'ce_teaser_element',
        '',
    ];
    $GLOBALS['TCA']['tt_content']['types']['ce_teaser_element'] = [
        'showitem' => '
            ' . $coreTabGeneral
                . $corePaletteGeneral
                . $corePaletteHeader
                . 'bodytext,'
            . $coreTabImages
                . 'image,'
            . $coreTabAppearance
                . $corePaletteFrames
                . $corePaletteAppearanceLinks
            . $coreTabLanguage
                . $corePaletteLanguage
            . $coreTabAccess
                . $corePaletteHidden
                . $corePaletteAccess
            . $coreTabCategories
                . 'categories,'
            . $coreTabNotes
                . 'rowDescription,'
            . $coreTabExtended
            .'
        ',
        'columnsOverrides' => [
            'bodytext' => [
                'config' => [
                    'enableRichtext' => true,
                    'required' => true,
                ],
            ],
            'header' => [
                'config' => [
                    'required' => true,
                ],
            ],
            'image' => [
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
                    'maxitems' => 1,
                ], $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
            ],
        ],
    ];

    /*
     * CType 'ce_teaser_image'
     */
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
        $projectLanguageFilePrefix . 'tt_content.CType.ce_teaser_image.title',
        'ce_teaser_image',
        '',
    ];
    $GLOBALS['TCA']['tt_content']['types']['ce_teaser_image'] = [
        'showitem' => '
            ' . $coreTabGeneral
                . $corePaletteGeneral
                . $corePaletteHeader
                . 'bodytext,'
                . 'tx_t3ocesitepackage_content_elements,'
                . '--palette--;;sectionlink,'
            . $coreTabMedia
                . 'image,'
                . 'assets;' . $projectLanguageFilePrefix . 'tt_content.CType.ce_teaser_image.assets_formlabel,'
            . $coreTabAppearance
                . $corePaletteFrames
                . $corePaletteAppearanceLinks
            . $coreTabLanguage
                . $corePaletteLanguage
            . $coreTabAccess
                . $corePaletteHidden
                . $corePaletteAccess
            . $coreTabCategories
                . 'categories,'
            . $coreTabNotes
                . 'rowDescription,'
            . $coreTabExtended
            .'
        ',
        'columnsOverrides' => [
            'assets' => [
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('assets', [
                ], $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
            ],
            'bodytext' => [
                'config' => [
                    'enableRichtext' => true,
                    'required' => true,
                ],
            ],
            'header' => [
                'config' => [
                    'required' => true,
                ],
            ],
            'image' => [
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
                    'maxitems' => 1,
                ], $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
            ],
        ],
    ];
});
