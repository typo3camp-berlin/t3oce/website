<?php

namespace T3oce\T3oceSitepackage\Domain\Model;

class Room extends \Evoweb\Sessionplaner\Domain\Model\Room
{
    /**
     * @var string
     */
    protected $streamLink;

    public function getStreamLink(): ?string
    {
        return $this->streamLink;
    }
}
