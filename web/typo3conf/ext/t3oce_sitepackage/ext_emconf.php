<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "t3oce_dsitepackage".
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 't3oce sitepackage',
    'description' => 'Configuration for t3oce website',
    'category' => 'backend',
    'version' => '0.0.0',
    'state' => 'stable',
    'uploadfolder' => false,
    'createDirs' => '',
    'clearcacheonload' => true,
    'author' => 'Jasmina Ließmann',
    'author_email' => '',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0 - 10.4.99',
        ],
        'conflicts' => [],
        'suggests' => []
    ],
];
