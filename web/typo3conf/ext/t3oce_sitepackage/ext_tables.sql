#
# Add columns to table 'tt_content'
#
CREATE TABLE tt_content (
    tx_t3ocesitepackage_bodytext2 mediumtext,
    tx_t3ocesitepackage_bodytext3 mediumtext,
    tx_t3ocesitepackage_content_elements int(11) DEFAULT '0' NOT NULL,
    tx_t3ocesitepackage_sectionlink_title varchar(255) DEFAULT '' NOT NULL,
    tx_t3ocesitepackage_sectionlink_url varchar(1024) DEFAULT '' NOT NULL,
);

#
# Table structure for table 'tx_t3ocesitepackage_domain_model_content'
#
CREATE TABLE tx_t3ocesitepackage_domain_model_content (
    uid int(11) NOT NULL auto_increment,
    pid int(11) DEFAULT '0' NOT NULL,

    tstamp int(11) DEFAULT '0' NOT NULL,
    crdate int(11) DEFAULT '0' NOT NULL,
    cruser_id int(11) DEFAULT '0' NOT NULL,
    deleted tinyint(4) DEFAULT '0' NOT NULL,
    hidden tinyint(4) DEFAULT '0' NOT NULL,
    starttime int(11) DEFAULT '0' NOT NULL,
    endtime int(11) DEFAULT '0' NOT NULL,
    sorting int(11) DEFAULT '0' NOT NULL,

    sys_language_uid int(11) DEFAULT '0' NOT NULL,
    l10n_parent int(11) DEFAULT '0' NOT NULL,
    l10n_source int(11) DEFAULT '0' NOT NULL,
    l10n_diffsource mediumblob,

    t3ver_oid int(11) DEFAULT '0' NOT NULL,
    t3ver_id int(11) DEFAULT '0' NOT NULL,
    t3ver_wsid int(11) DEFAULT '0' NOT NULL,
    t3ver_label varchar(255) DEFAULT '' NOT NULL,
    t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
    t3ver_stage int(11) DEFAULT '0' NOT NULL,
    t3ver_count int(11) DEFAULT '0' NOT NULL,
    t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
    t3ver_move_id int(11) DEFAULT '0' NOT NULL,
    t3_origuid int(11) DEFAULT '0' NOT NULL,

    parentid int(11) DEFAULT '0' NOT NULL,
    parenttable text,

    bodytext mediumtext,
    header varchar(255) DEFAULT '' NOT NULL,
    link_title varchar(255) DEFAULT '' NOT NULL,
    link_url varchar(1024) DEFAULT '' NOT NULL,

    PRIMARY KEY (uid),
    KEY parent (pid)
);

CREATE TABLE tx_sessionplaner_domain_model_room
(
    tx_t3ocesitepackage_stream_link varchar(255),
);