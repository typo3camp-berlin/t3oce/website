<?php
defined('TYPO3_MODE') or die();

// Add page TSConfig
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '@import "EXT:t3oce_sitepackage/Configuration/TSconfig/Page.tsconfig"'
);

// Add user TSConfig
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig(
    '@import "EXT:t3oce_sitepackage/Configuration/TSconfig/User.tsconfig"'
);

// Configurations for CKEditor
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['default'] = 'EXT:t3oce_sitepackage/Configuration/Yaml/Editor/Default.yaml';

\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
    ->registerImplementation(
        \Evoweb\Sessionplaner\Domain\Model\Room::class,
        \T3oce\T3oceSitepackage\Domain\Model\Room::class
    );