<?php

defined('TYPO3_MODE') || exit('Access denied.');

/**
 * Loads and sets configurations dependent to several conditions
 */
class AdditionalConfiguration
{
    public function connectToDatabase(): self
    {
        $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['dbname'] = getenv('TYPO3_DB_DATABASE');
        $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['user'] = getenv('TYPO3_DB_USERNAME');
        $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['host'] = getenv('TYPO3_DB_HOST');
        $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['password'] = getenv('TYPO3_DB_PASSWORD');
        return $this;
    }

    /**
     * Include additional configurations by TYPO3_CONTEXT server variable
     */
    public function loadContextDependentConfigurations(): self
    {
        $currentContext = \TYPO3\CMS\Core\Core\Environment::getContext();
        if ($currentContext->isProduction()) {
            $GLOBALS['TYPO3_CONF_VARS']['LOG']['TYPO3']['CMS']['deprecations']['writerConfiguration'][\TYPO3\CMS\Core\Log\LogLevel::NOTICE] = [];
            $GLOBALS['TYPO3_CONF_VARS']['SYS']['enableDeprecationLog'] = false;
            $GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_password'] = getenv('TYPO3_MAIL');
            return $this;
        }
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'] .= ' (' . (string)$currentContext . ')';
        $orderedListOfContextNames = [];
        do {
            $orderedListOfContextNames[] = (string)$currentContext;
        } while (($currentContext = $currentContext->getParent()));
        $orderedListOfContextNames = array_reverse($orderedListOfContextNames);
        foreach ($orderedListOfContextNames as $contextName) {
            $contextConfigFilePath = TYPO3\CMS\Core\Core\Environment::getProjectPath() . '/AdditionalConfiguration/' . $contextName . 'Context.php';
            if (file_exists($contextConfigFilePath)) {
                require($contextConfigFilePath);
            }
        }
        return $this;
    }
}

$additionalConfiguration = new \AdditionalConfiguration();
$additionalConfiguration->connectToDatabase()
    ->loadContextDependentConfigurations();
