<?php

$GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] = '*';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['sqlDebug'] = 1;
$GLOBALS['TYPO3_CONF_VARS']['SYS']['displayErrors'] = 1;

$GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_path'] = '/usr/local/bin/';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['systemLogLevel'] = 2;
$GLOBALS['TYPO3_CONF_VARS']['SYS']['systemLog'] = 'file,/Users/danielgoerz/projects/vhosts/t3oce/dev/log/typo3.log,2';

$GLOBALS['TYPO3_CONF_VARS']['SYS']['systemLocale'] = 'de_DE.UTF-8';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['enableDeprecationLog'] = true;

$GLOBALS['TYPO3_CONF_VARS']['BE']['sessionTimeout'] = 60 * 60 * 24 * 365;
$GLOBALS['TYPO3_CONF_VARS']['BE']['lockSSL'] = false;
$GLOBALS['TYPO3_CONF_VARS']['BE']['debug'] = true;

$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport'] = 'sendmail';