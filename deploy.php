<?php
namespace Deployer;

inventory('build/deployer/hosts.yaml');

require 'recipe/common.php';
require 'build/deployer/task/typo3_console.php';
require 'build/deployer/task/t3oce.php';

task('release', [
    'deploy:info',
    'deploy:prepare',
    'deploy:release',
    't3oce:rsync',
    'deploy:shared',
    'deploy:symlink',
    'typo3:flush:caches',
    'typo3:setup:extensions',
    'typo3:update:databaseschema',
    'typo3:create_default_folders'
]);

task('deploy', [
    't3oce:assure_connection',
    'release',
    'cleanup',
    'success'
]);
