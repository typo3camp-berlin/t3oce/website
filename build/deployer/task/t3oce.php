<?php

namespace Deployer;

task('t3oce:rsync', function () {
    upload(__DIR__ . '/../../../',
        '{{release_path}}',
        [
            'options' => [
                "--exclude '.git'",
                "--exclude 'build'",
                "--exclude '.env'",
            ]
        ]
    );
});

task('t3oce:assure_connection', function (){
    $user = trim(get('user')) ?: '';
    if (!empty($user)) {
        $user = $user . '@';
    }
    $hostname = \Deployer\Task\Context::get()->getHost()->getRealHostname();
    $sshOptions = ['-A', '-q', '-o BatchMode=yes'];
    runLocally('ssh ' . implode(' ', $sshOptions) . ' ' . escapeshellarg($user . $hostname).' exit;');
});