<?php

namespace Deployer;

/**
 * Inspired by https://github.com/helhum/typo3-deployer-recipe
 *
 * Notable differences:
 *  * not bound to 'transfer' task
 *  * exports TYPO3_CONTEXT because the config switch relies on that env
 *  * reduced to the needs of the project
 */

set('source_path', function () {
    $sourcePath = '{{build_path}}/current';
    if (!has('build_path') && !Deployer::hasDefault('build_path')) {
        if (!file_exists(\getcwd() . '/deploy.php')) {
            throw new ConfigurationException('Could not determine path to deployment source directory ("source_path")', 1512317992);
        }
        $sourcePath = getcwd();
    }
    return $sourcePath;
});

// Fetch composer.json and store it in array for later use
set('composer_config', function () {
    $composerJsonPath = parse('{{source_path}}/composer.json');
    if (!file_exists($composerJsonPath)) {
        // If we don't find a composer.json file, we assume the root dir to be the release path
        return null;
    }
    return \json_decode(\file_get_contents($composerJsonPath), true);
});
// Extract bin-dir from composer config
set('composer_config/bin-dir', function() {
    $binDir = '{{release_path}}/vendor/bin';
    $composerConfig = get('composer_config');
    if (isset($composerConfig['config']['bin-dir'])) {
        $binDir = '{{release_path}}/' . $composerConfig['config']['bin-dir'];
    }
    return $binDir;
});

set('bin/typo3cms', function () {
    if (test('[ -f {{composer_config/bin-dir}}/typo3cms ]')) {
        return '{{bin/php}} {{composer_config/bin-dir}}/typo3cms';
    }
    if (test('[ -f {{composer_config/bin-dir}}/typo3console ]')) {
        return '{{bin/php}} {{composer_config/bin-dir}}/typo3console';
    }
    if (test('[ -f {{release_path}}/typo3cms ]')) {
        return '{{bin/php}} {{release_path}}/typo3cms';
    }
    return null;
});

function runConsole($command, array $arguments = [])
{
    if (get('bin/typo3cms') === null) {
        output()->writeln(sprintf('<comment>Could not detect TYPO3 Console binary, skipping "%s"</comment>', $command));
        output()->writeln('<comment>Consider defining the path as "bin/typo3cms" within your Deployer configuration.</comment>');
        return '';
    }
    array_unshift($arguments, $command);
    return run('export TYPO3_CONTEXT={{typo3_context}} && {{bin/typo3cms}} ' . implode(' ', array_map('escapeshellarg', $arguments)));
}

/**
 * Individual (reusable tasks)
 */
set('command', '');
set('arguments', []);
task('typo3:console', function () {
    $command = get('command');
    $arguments = get('arguments');
    if (is_string($arguments)) {
        $arguments = explode(' ', $arguments);
    }
    output()->writeln(runConsole($command, $arguments));
})->desc('Run any TYPO3 Console command');

task('typo3:flush:caches', function () {
    runConsole('cache:flush');
})->desc('Flush caches');

task('typo3:create_default_folders', function () {
    runConsole('install:fixfolderstructure');
})->desc('Creates TYPO3 default folder structure');

task('typo3:update:databaseschema', function () {
    runConsole('database:updateschema', ['field.add,field.change,table.add,table.change']);
})->desc('Update database schema');

task('typo3:setup:extensions', function () {
    runConsole('extension:setupactive');
})->desc('Set up TYPO3 extensions');
